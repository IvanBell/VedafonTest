<?php

//error_reporting(0);

require_once __DIR__ . '/../vendor/autoload.php';

$db = new \VedafonTest\DB(array(
    'user' => 'root',
    'password' => '7550055',
    'database' => 'test_task'
));


$allowed_methods = array('Table', 'SessionSubscribe', 'PostNews');

$allowed_tables = array('News', 'Session');

$response = new \VedafonTest\JsonResponse();


if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    $response->send_error('Method not allowed');
    return;
}

$uri = explode('/', substr($_SERVER['REQUEST_URI'], 1));
$prefix = isset($uri[0]) ? $uri[0] : null;
$method = isset($uri[1]) ? $uri[1] : null;

if ($prefix != 'api' || count($uri) < 2) {
    $response->send_error('Path not found');
    return;
}

if (!in_array($method, $allowed_methods)) {
    $response->send_error('Method not found');
    return;
}


if (!$db->connection) {
    $response->send_error('Database connection error');
    return;
}

if ($method == 'Table') {

    $table = isset($_POST['table']) ? $db->escape_string($_POST['table']) : false;
    $id = isset($_POST['id']) ? intval($_POST['id']) : false;

    if (!in_array($table, $allowed_tables)) {
        $response->send_error('Invalid table name');
        return;
    }
    if ($id) {
        if (!($id > 0)) {
            $response->send_error('Invalid record id');
            return;
        }
        $data = $db->get_row(sprintf("SELECT * FROM `%s` WHERE id=%d", $table, $id));

    } else {
        $data= $db->get_table(sprintf("SELECT * FROM `%s`", $table));
    }

    if ($table == 'Session')
    {
        $payload=array();
        if (is_array($data))
        {
            // all records
            foreach ($data as $row)
            {
                $row->Speakers=$db->get_table(sprintf("SELECT * FROM Speaker WHERE ID IN(SELECT SpeakerID FROM SessionSpeaker WHERE SessionID=%d)",$row->ID));
                $payload[]=$row;
            }
            $data=$payload;
        } else {
            // single record
            $data->Speakers=$db->get_table(sprintf("SELECT * FROM Speaker WHERE ID IN(SELECT SpeakerID FROM SessionSpeaker WHERE SessionID=%d)",$id));
        }
    }
    $response->payload=$data;
    $response->send();
    return;


}

if ($method == 'PostNews') {

    $email = isset($_POST['userEmail']) ? $db->escape_string($_POST['userEmail']) : false;
    $title = isset($_POST['newsTitle']) ? $db->escape_string($_POST['newsTitle']) : false;
    $message = isset($_POST['newsMessage']) ? $db->escape_string($_POST['newsMessage']) : false;

    // Simple validation. More complex validation must be used in production
    if (!($email && $title && $message)) {
        $response->send_error('Invalid method arguments');
        return;
    }

    if (strlen($title) < 1) {
        $response->send_error('Wrong title value');
        return;
    }
    if (strlen($message) < 1) {
        $response->send_error('Wrong message value');
        return;
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response->send_error('Wrong email value');
        return;
    }

    $participant_id = $db->get_value(sprintf("SELECT ID FROM Participant WHERE email='%s'", $email));
    if (!$participant_id) {
        $response->send_error('Participant not found');
        return;
    }
    $news_id = $db->get_value(sprintf("SELECT ID FROM News WHERE ParticipantID=%d AND NewsTitle='%s' AND NewsMessage='%s'", $participant_id, $title, $message));

    if ($news_id) {
        $response->send_error('This news already exists');
        return;
    }
    $inserted = $db->query(sprintf("INSERT INTO News (ParticipantID,NewsTitle,NewsMessage) VALUES(%d,'%s','%s')",
        $participant_id, $title, $message));
    if ($inserted == 1) {
        $response->send('News added');
        return;
    } else {
        $response->send_error('Unexpected database error');
        return;
    }

}

if ($method == 'SessionSubscribe') {

    $email = isset($_POST['userEmail']) ? $db->escape_string($_POST['userEmail']) : false;
    $session_id = isset($_POST['sessionId']) ? intval($_POST['sessionId']) : false;

    // Simple validation. More complex validation must be used in production
    if (!($email && $session_id)) {
        $response->send_error('Invalid method arguments');
        return;
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response->send_error('Wrong email value');
        return;
    }
    $session = $db->get_row(sprintf("SELECT * FROM Session WHERE ID=%d", $session_id));
    if (!$session) {
        $response->send_error('Session not found');
        return;
    }

    $participant_id = $db->get_value(sprintf("SELECT ID FROM Participant WHERE email='%s'", $email));
    if (!$participant_id) {
        $response->send_error('Participant not found');
        return;
    }

    // in production environment it is better to do this actions in the stored procedure with transactions
    // and strict checking for overbooking
    $participants_count = $db->get_value(sprintf("SELECT count(*) FROM SessionParticipant WHERE SessionID=%d", $session_id));
    if ($participants_count >= $session->MaxParticipants) {
        $response->send_error('Sorry, registration finished');
        return;
    }
    $exists = $db->get_value(sprintf("SELECT count(*) FROM SessionParticipant WHERE ParticipantID=%d", $participant_id));
    if ($exists != 0) {
        $response->send_error('You already registered to this session');
        return;
    }
    $inserted = $db->query(sprintf("INSERT INTO SessionParticipant (SessionID,ParticipantID) VALUES(%d,%d)", $session_id, $participant_id));
    if ($inserted == 1) {
        $response->send_error('Thanks! You have successfully registered to the session');
        return;
    } else {
        $response->send_error('Unexpected database error');
        return;
    }
}



