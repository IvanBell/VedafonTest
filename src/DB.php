<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 13/05/2017
 * Time: 13:23
 */

namespace VedafonTest;


class DB
{

    public $connection;
    public $params;

    public function __construct($params = array())
    {
        $defaults = array(
            'host' => '127.0.0.1',
            'port' => '3306',
            'user' => 'test',
            'password' => '',
            'database' => 'test'
        );

        $this->params = array_merge($defaults, $params);
        try {
            $this->connection = mysqli_connect(
                $this->params['host'], $this->params['user'], $this->params['password'], $this->params['database'], $this->params['port']
            );
        } catch (\Exception $e) {
            $this->connection = false;
        }
    }

    public function get_table($query)
    {

        $result = $this->connection->query($query);
        $data = array();
        if ($result) {
            while ($row = $result->fetch_object()) {
                $data[] = $row;
            }
        } else {
            $data = false;
        }

        $result->close();

        return $data;
    }

    public function get_row($query)
    {
        $result = $this->connection->query($query);
        if ($result) {
            $row = $result->fetch_object();
        } else {
            $row = false;
        }

        $result->close();
        return $row;
    }

    public function get_value($query)
    {
        $result = $this->connection->query($query);
        if ($result->num_rows > 0) {
            $value = $result->fetch_array(MYSQLI_NUM);
            $value = $value[0];
        } else {
            $value = false;
        }
        $result->close();
        return $value;
    }

    public function query($query)
    {
        $result = $this->connection->query($query);
        if ($result) {
            return $this->connection->affected_rows;
        } else {
            return false;
        }
    }

    public function escape_string($string)
    {
        return $this->connection->escape_string($string);
    }


}