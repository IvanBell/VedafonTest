<?php

namespace VedafonTest;

class JsonResponse
{
    public $status;
    public $message;
    public $payload;

    public function __construct($status = 'ok', $message = '', $payload = array())
    {
        $this->status = $status;
        $this->message = $message;
        $this->payload = $payload;
    }

    public function send_error($message = 'unknown error', $payload = array())
    {
        $this->status = 'error';
        $this->payload = $payload;
        $this->message = $message;
        $this->send();
    }

    public function send($message=null)
    {
        header_remove();
        header('Content-Type: application/json');
        if ($message!=null)
        {
            $this->message=$message;
        }
        echo json_encode(array(
            'status' => $this->status,
            'message' => $this->message,
            'payload' => $this->payload
        ));
    }
}